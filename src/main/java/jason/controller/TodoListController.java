package jason.controller;

import jason.beans.TodoItem;
import jason.service.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoListController {

    @Autowired
    private TodoListService todoListService;

    @GetMapping
    public List<TodoItem> getAllTodoItems() {
        return todoListService.findAllTodoItems();
    }

    @PostMapping
    public TodoItem createTodoItem(@RequestBody TodoItem item) {
        if (item == null) return null;
        return todoListService.saveTodoItem(item);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeTodoItemById(@PathVariable("id") Long id) {
        if (id == null) return;
        todoListService.removeTodoItemById(id);
    }

    @PutMapping("/{id}")
    public TodoItem updateTodoItemById(@PathVariable("id") Long id, @RequestBody TodoItem item) {
        if (id == null || item == null) return null;
        return todoListService.updateTodoItemById(id, item);
    }

    @GetMapping("/{id}")
    public TodoItem getDoneTodoItemById(@PathVariable("id") Long id) {
        if (id == null) return null;
        return todoListService.findDoneTodoItemById(id);
    }
}
