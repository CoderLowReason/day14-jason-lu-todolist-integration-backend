package jason.repository;

import jason.beans.TodoItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoListRepository extends JpaRepository<TodoItem, Long> {

}
