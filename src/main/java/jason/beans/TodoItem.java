package jason.beans;

import javax.persistence.*;

@Entity
@Table(name = "todoList")
public class TodoItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String content;
    private Boolean done;
    private String description;

    public Long getId() {
        return id;
    }

    public TodoItem() {
    }

    public TodoItem(Long id, String content, Boolean done, String description) {
        this.id = id;
        this.content = content;
        this.done = done;
        this.description = description;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
