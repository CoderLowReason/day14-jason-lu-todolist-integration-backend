package jason.service;

import jason.beans.TodoItem;
import jason.repository.TodoListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoListService {

    @Autowired
    private TodoListRepository todoListRepository;

    public List<TodoItem> findAllTodoItems() {
        return todoListRepository.findAll();
    }

    public TodoItem saveTodoItem(TodoItem item) {
        return todoListRepository.save(item);
    }

    public void removeTodoItemById(Long id) {
        todoListRepository.deleteById(id);
    }

    public TodoItem updateTodoItemById(Long id, TodoItem updateItem) {
        updateItem.setId(id);
        return todoListRepository.save(updateItem);
    }

    public TodoItem findDoneTodoItemById(Long id) {
        TodoItem doneTodoItem = todoListRepository.findById(id).orElse(null);
        assert doneTodoItem != null;
        if (doneTodoItem.getDone()) {
            return doneTodoItem;
        } else {
            return null;
        }
    }
}
