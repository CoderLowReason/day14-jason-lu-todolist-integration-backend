package jason.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jason.beans.TodoItem;
import jason.repository.TodoListRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoListControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TodoListRepository todoListRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        todoListRepository.deleteAll();
    }

    @Test
    void should_get_all_todoList_items_when_getAllTodoItems() throws Exception {
        //given
        TodoItem todoItem = new TodoItem(null, "Content 1", false, "Content 1 Description");
        todoListRepository.save(todoItem);
        //when
        //then
        mockMvc.perform(get("/todos").contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].content").value("Content 1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].done").value(false))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].description").value("Content 1 Description"));
    }

    @Test
    void should_create_a_todo_item_when_createTodoItem_given_a_item() throws Exception {
        //given
        TodoItem todoItem = new TodoItem(null, "Content 1", false, "Content 1 Description");
        String itemObjectString = objectMapper.writeValueAsString(todoItem);
        //when
        //then
        mockMvc.perform(post("/todos").contentType(MediaType.APPLICATION_JSON).content(itemObjectString))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.content").value("Content 1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.done").value(false))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.description").value("Content 1 Description"));
        List<TodoItem> items = todoListRepository.findAll();
        assertEquals(1, items.size());
        assertEquals(items.get(0).getContent(), todoItem.getContent());
        assertEquals(items.get(0).getDescription(), todoItem.getDescription());
    }

    @Test
    void should_delete_a_todo_item_when_deleteTodoItemById_given_a_id() throws Exception {
        //given
        TodoItem todoItem = new TodoItem(null, "Content 1", false, "Content 1 Description");
        TodoItem savedItem = todoListRepository.save(todoItem);
        //when
        //then
        mockMvc.perform(delete("/todos/{id}", savedItem.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));
        List<TodoItem> items = todoListRepository.findAll();
        assertEquals(0, items.size());
    }

    @Test
    void should_update_a_todo_item_when_updateTodoItemById_given_a_id_and_a_item() throws Exception {
        //given
        TodoItem todoItem = new TodoItem(null, "Content 1", false, "Content 1 Description");
        TodoItem savedItem = todoListRepository.save(todoItem);
        TodoItem updateItem = new TodoItem(null, "update Content 1", true, "update Content 1 Description");
        String updateItemString = objectMapper.writeValueAsString(updateItem);
        //when
        //then
        mockMvc.perform(put("/todos/{id}", savedItem.getId()).contentType(MediaType.APPLICATION_JSON).content(updateItemString))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(savedItem.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.content").value(updateItem.getContent()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.done").value(updateItem.getDone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.description").value(updateItem.getDescription()));
    }

    @Test
    void should_get_a_done_todo_item_when_getDoneTodoItemById_given_a_id() throws Exception {
        //given
        TodoItem doneTodoItem = new TodoItem(null, "Content 1", true, "Content 1 Description");
        TodoItem undoneTodoItem = new TodoItem(null, "Content 2", false, "Content 2 Description");

        TodoItem savedDoneTodoItem = todoListRepository.save(doneTodoItem);
        todoListRepository.save(undoneTodoItem);

        //when
        //then
        mockMvc.perform(get("/todos/{id}", savedDoneTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(savedDoneTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.content").value(savedDoneTodoItem.getContent()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.done").value(savedDoneTodoItem.getDone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.description").value(savedDoneTodoItem.getDescription()));
    }
}
